#!/bin/sh

apt-get update -qq && apt-get install -y build-essential vim

apt-get install -y default-libmysqlclient-dev

curl -sL https://deb.nodesource.com/setup_11.x | bash - 
apt-get install -y nodejs

bundle install --path vendor/bundle
bundle exec puma -C config/puma.rb
